// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package db_test

import (
	"budgy/config"
	"budgy/db"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"os"
	"path/filepath"
	"testing"
)

func TestMainDbGetPath(t *testing.T) {
	dataDirPath := viper.GetString("data-dir")
	filename := config.APPNAME + ".db"

	var tests = []struct {
		Name     string
		Path     string
		wantPath string
	}{
		{
			Name:     "Empty",
			Path:     "",
			wantPath: filename,
		},
		{
			Name:     "Relative path",
			Path:     "data",
			wantPath: "data/" + filename,
		},
		{
			Name:     "Relative path with tailling /",
			Path:     "data/",
			wantPath: "data/" + filename,
		},
		{
			Name:     "Relative path with dot",
			Path:     "../data",
			wantPath: "../data/" + filename,
		},
		{
			Name:     "Absolute path",
			Path:     "/var/data",
			wantPath: "/var/data/" + filename,
		},
		{
			Name:     "Absolute path with tailling /",
			Path:     "/var/data/",
			wantPath: "/var/data/" + filename,
		},
		{
			Name:     "Root",
			Path:     "/",
			wantPath: "/" + filename,
		},
	}

	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			asserter := assert.New(t)
			viper.Set("data-dir", tt.Path)
			asserter.Exactly(tt.wantPath, db.MainDbGetPath())
		})
	}

	viper.Set("data-dir", dataDirPath)
}

func TestMainDbGetAbsPath(t *testing.T) {
	dataDirPath := viper.GetString("data-dir")
	filename := config.APPNAME + ".db"
	cwd, _ := os.Getwd()

	var tests = []struct {
		Name     string
		Path     string
		wantPath string
	}{
		{
			Name:     "Empty",
			Path:     "",
			wantPath: cwd + "/" + filename,
		},
		{
			Name:     "Relative path",
			Path:     "data",
			wantPath: cwd + "/data/" + filename,
		},
		{
			Name:     "Relative path with tailling /",
			Path:     "data/",
			wantPath: cwd + "/data/" + filename,
		},
		{
			Name:     "Relative path with dot",
			Path:     "../data",
			wantPath: filepath.Dir(cwd) + "/data/" + filename,
		},
		{
			Name:     "Absolute path",
			Path:     "/var/data",
			wantPath: "/var/data/" + filename,
		},
		{
			Name:     "Absolute path with tailling /",
			Path:     "/var/data/",
			wantPath: "/var/data/" + filename,
		},
		{
			Name:     "Root",
			Path:     "/",
			wantPath: "/" + filename,
		},
	}

	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			asserter := assert.New(t)
			viper.Set("data-dir", tt.Path)
			asserter.Exactly(tt.wantPath, db.MainDbGetAbsPath())
		})
	}

	viper.Set("data-dir", dataDirPath)
}
