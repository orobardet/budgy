// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package db

import (
	"budgy/config"
	"budgy/models"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/spf13/viper"
	"os"
	"path/filepath"
)

// MainDbGetPath returns the full path of the main database file
func MainDbGetPath() string {
	dataDirPath := viper.GetString("data-dir")

	if dataDirPath == "" {
		return filepath.Clean(config.MainDBFileName)
	}

	return filepath.Clean(dataDirPath + string(os.PathSeparator) + config.MainDBFileName)
}

// MainDbGetAbsPath returns the absolute path of the main database file
func MainDbGetAbsPath() string {
	mainDbPath, err := filepath.Abs(MainDbGetPath())
	if err != nil {
		panic(err)
	}

	return mainDbPath
}

// MainDbCheckMinimalAvailability checks if minimal requirements for a valid main database are met
// - The database file must exists
// - It must be an accessible sqlite file
// - At least one admin user should exists in the database
func MainDbCheckMinimalAvailability() bool {
	mainDbPath := MainDbGetAbsPath()

	// The data directory path should exist and must not be a directory
	if fi, err := os.Stat(mainDbPath); os.IsNotExist(err) || fi.IsDir() {
		return false
	}

	// Try to open the database
	db, err := gorm.Open("sqlite3", mainDbPath)
	if err != nil {
		fmt.Println(err)
		return false
	}
	defer db.Close()

	var adminUser models.User

	// Is there an user table?
	if !db.HasTable(&adminUser) {
		return false
	}

	// Is there at least one admin user?
	if err := db.First(&adminUser, "admin = ?", true).Error; err != nil {
		return false
	}

	return true
}
