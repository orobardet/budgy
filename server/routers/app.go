// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package routers

import (
	"budgy/config"
	"budgy/i18n"
	"budgy/server/controllers"
	"github.com/GeertJohan/go.rice"
	"github.com/foolin/gin-template"
	"github.com/foolin/gin-template/supports/gorice"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/memstore"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"html/template"
)

// SubrouterInitier callback type, registered by every app's router to publish themself to the main application router
type SubrouterInitier func(*gin.RouterGroup)

var registeredSubrouters = map[string]SubrouterInitier{}

// RegisterAppSubRouteur allows each application's router to register the callback function that will delcare the
// sub-router
func RegisterAppSubRouteur(path string, initHandler SubrouterInitier) {
	registeredSubrouters[path] = initHandler
}

func initTemplateManager(router *gin.Engine) {
	gintemplate.Default()
	templateConfig := gintemplate.DefaultConfig
	templateConfig.DisableCache = true
	templateConfig.Funcs = template.FuncMap{
		"localize": func(format string, a ...interface{}) string {
			return i18n.Localizer.Sprintf(format, a...)
		},
		"_": func(format string, a ...interface{}) string {
			return i18n.Localizer.Sprintf(format, a...)
		},
	}
	router.HTMLRender = gorice.NewWithConfig(rice.MustFindBox("../views"), templateConfig)
}

func initStatics(router *gin.Engine) {
	staticBox, _ := rice.FindBox("../static")
	router.StaticFS("/static/", staticBox.HTTPBox())
}

func initSessionManager(router *gin.Engine) {
	router.Use(sessions.Sessions(config.APPNAME, memstore.NewStore([]byte("session_secret"))))
}

// InitApp initialize a gin's Engine, populate routers and sub engine (template, statics, session, ...)
// The returns object is a gin app ready to serve
func InitApp() *gin.Engine {
	gin.ForceConsoleColor()
	controllers.AddGlobalTemplateData("dev", true)
	// Set release mode for gin
	if isDebug := viper.GetBool("debug"); config.RELEASE && !isDebug {
		controllers.AddGlobalTemplateData("dev", false)
		gin.SetMode(gin.ReleaseMode)
	}

	// Init Gin
	appRouter := gin.New()
	appRouter.Use(gin.Logger())
	appRouter.Use(gin.Recovery())

	// Init template engine
	initTemplateManager(appRouter)

	// Static files
	initStatics(appRouter)

	// Init session
	initSessionManager(appRouter)

	appRouter.Use(controllers.App.IsUserLogged)

	// App index
	appRouter.GET("/", controllers.App.Index)
	// Sign in
	appRouter.GET("/signin", controllers.App.SignInForm)
	// Changelog
	appRouter.GET("/changelog", controllers.App.Changelog)

	// Init subrouters
	for path, initier := range registeredSubrouters {
		group := appRouter.Group(path)
		initier(group)
	}

	return appRouter
}
