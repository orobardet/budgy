// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package controllers

import (
	"budgy/config"
	"github.com/gin-gonic/gin"
)

var globalTemplateData = gin.H{}

type commonController struct {
	Section    string
	SubSection string
}

// AddGlobalTemplateData adds a new key/value data to the global templates data.
// Global templates data are populated in every template rendering.
func AddGlobalTemplateData(name string, value interface{}) {
	globalTemplateData[name] = value
}

func (c *commonController) H(local gin.H) gin.H {
	merged := gin.H{}
	for k, v := range globalTemplateData {
		merged[k] = v
	}

	if c.Section != "" {
		merged["section"] = c.Section
	}

	if c.SubSection != "" {
		merged["subSection"] = c.SubSection
	}

	for k, v := range local {
		merged[k] = v
	}
	return merged
}

func init() {
	AddGlobalTemplateData("app", gin.H{
		"name":    config.APPNAME,
		"version": config.VERSION,
	})
	AddGlobalTemplateData("showNavMenu", true)
}
