// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package controllers

import (
	"budgy/db"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"net/http"
	"regexp"
)

type appController struct {
	commonController
}

// App is the main application controller
var App = &appController{}

var guestAllowedPath []*regexp.Regexp

func init() {
	allowed := []string{
		"^/signin$",
		"^/setup$",
		"^/setup/.*$",
		"^/changelog$",
	}

	for _, path := range allowed {
		guestAllowedPath = append(guestAllowedPath, regexp.MustCompile(path))
	}
}

// IsGuestAllowedPath tells if the given path can be accessed by a guest (i.e. without beeing signed in)
func IsGuestAllowedPath(path string) bool {
	for _, rex := range guestAllowedPath {
		if rex.MatchString(path) {
			return true
		}
	}

	return false
}

// IsUserLogged tells if a user is currently logged
func (ctrl appController) IsUserLogged(ctx *gin.Context) {
	if IsGuestAllowedPath(ctx.Request.URL.Path) {
		return
	}

	session := sessions.Default(ctx)
	username := session.Get("username")
	if username == nil {
		ctx.Redirect(http.StatusFound, "/signin")
		ctx.Abort()
		return
	}
}

// Index of application (Homepage)
func (ctrl appController) Index(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "index", ctrl.H(gin.H{
		"title": "Index title!",
	}))
}

// SignInForm handle the sign in form page
func (ctrl appController) SignInForm(ctx *gin.Context) {
	// Check if there is no database or no minimal data, and if so redirect to setup
	if !db.MainDbCheckMinimalAvailability() {
		ctx.Redirect(http.StatusFound, "/setup")
		ctx.Abort()
		return
	}

	ctx.HTML(http.StatusOK, "signin", ctrl.H(gin.H{}))
}

// Changelog page of the application
func (ctrl appController) Changelog(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "changelog", ctrl.H(gin.H{
		"title": "Change log",
	}))
}
