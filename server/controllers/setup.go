// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package controllers

import (
	"budgy/db"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

type setupStepHeader struct {
	Title string
}

type setupStep struct {
	Header    *setupStepHeader
	Name      string
	Title     string
	Icon      string
	NextLabel string
}

type setupController struct {
	commonController
	setupSteps []*setupStep
}

// Setup controller instance
var Setup = &setupController{
	commonController: commonController{
		Section: "setup",
	},
	setupSteps: []*setupStep{
		{
			Name:      "welcome",
			Title:     "Welcome",
			Icon:      "info",
			NextLabel: "Next step",
		},
		{
			Header: &setupStepHeader{
				Title: "Data",
			},
			Name:      "create-database",
			Title:     "Create database",
			Icon:      "storage",
			NextLabel: "Next step",
		},
		{
			Name:      "create-user",
			Title:     "Create first user",
			Icon:      "person_add",
			NextLabel: "Next step",
		},
		{
			Header: &setupStepHeader{
				Title: "",
			},
			Name:      "finished",
			Title:     "Finished",
			Icon:      "flag",
			NextLabel: "",
		},
	},
}

func (ctrl *setupController) GetSetupStep(stepName string) (currentStep *setupStep, nextStep *setupStep) {
	currentStep = nil
	nextStep = nil
	for index, step := range ctrl.setupSteps {
		if step.Name == stepName {
			currentStep = step
			if len(ctrl.setupSteps) > index+1 {
				nextStep = ctrl.setupSteps[index+1]
			}
		}
	}

	return
}

func (ctrl *setupController) Index(ctx *gin.Context) {
	// Don't allow setup if a valid database exists
	if db.MainDbCheckMinimalAvailability() {
		ctx.Redirect(http.StatusFound, "/")
		ctx.Abort()
		return
	}

	currentStepName := ctx.Param("stepName")
	if currentStepName == "" {
		currentStepName = "welcome"
	}
	currentStep, nextStep := ctrl.GetSetupStep(currentStepName)
	if currentStep == nil {
		ctx.Header("Error", fmt.Sprintf("Unknown setup step '%s'", currentStepName))
		ctx.Redirect(http.StatusFound, "/setup")
		ctx.Abort()
		return
	}

	nextStepLink := ""
	if nextStep != nil {
		nextStepLink = "/setup/" + nextStep.Name
	}

	ctx.HTML(http.StatusOK, "setup/index", ctrl.H(gin.H{
		"title":        "Setup",
		"showSidebar":  true,
		"showNavMenu":  false,
		"setupSteps":   &ctrl.setupSteps,
		"currentStep":  currentStep,
		"nextStepLink": nextStepLink,
	}))
}
