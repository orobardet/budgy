// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"budgy/importer/qif"
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

// importCmd represents the importer command
var importCmd = &cobra.Command{
	Use:   "import <file to import>",
	Short: "Import a external file",
	Long: `Import a file from another budget management software.
For now, only QIF files are supported.`,
	Run:  importExternalFile,
	Args: cobra.ExactArgs(1),
}

func init() {
	rootCmd.AddCommand(importCmd)
}

// checkImportFilePath validate that importFilePath exists and is a regular file
// If not, output an error message on stderr and exist with code 1
func checkImportFilePath(importFilePath string) {
	// Does the file exists?
	if _, err := os.Stat(importFilePath); os.IsNotExist(err) {
		fmt.Fprintf(os.Stderr, "File %s does not exists.\n", importFilePath)
		os.Exit(1)
	}

	// Is it a regular file?
	fi, err := os.Stat(importFilePath)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	if mode := fi.Mode(); !mode.IsRegular() {
		fmt.Fprintf(os.Stderr, "File %s is not a regular file.\n", importFilePath)
		os.Exit(1)
	}
}

// importExternalFile is the main function for command "importer"
func importExternalFile(cmd *cobra.Command, args []string) {
	importFilePath := args[0]
	checkImportFilePath(importFilePath)
	fmt.Printf("Importing %s...\n", importFilePath)

	importer := qif.NewImporter()
	err := importer.ImportFile(importFilePath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
}
