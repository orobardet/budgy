// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"github.com/spf13/afero"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"testing"
)

func testCaptureOutput(process func()) (string, string) {
	oldStdout := os.Stdout
	oldStderr := os.Stderr
	defer func() {
		os.Stdout = oldStdout
		os.Stderr = oldStderr
	}()

	rout, wout, _ := os.Pipe()
	os.Stdout = wout
	rerr, werr, _ := os.Pipe()
	os.Stderr = werr

	process()

	wout.Close()
	werr.Close()
	stdout, _ := ioutil.ReadAll(rout)
	stderr, _ := ioutil.ReadAll(rerr)

	return string(stdout), string(stderr)
}

func Test_initConfig_Defaults(t *testing.T) {
	ensure := assert.New(t)

	initConfig()

	ensure.False(viper.GetBool("debug"))
	ensure.Exactly(viper.Get("config-file"), "")
	ensure.Exactly(viper.Get("data-dir"), "data/")
}

func Test_initConfig_DebugMessages(t *testing.T) {
	ensure := assert.New(t)

	fs := afero.NewMemMapFs()
	viper.SetFs(fs)
	afero.WriteFile(fs, "./budgy.yaml", []byte(`data-dir: /test/is/ok`), 0644)

	cfgFile = "./budgy.yaml"
	viper.Set("debug", true)
	stdout, _ := testCaptureOutput(func() {
		initConfig()
	})
	ensure.Equal("Using config file: ./budgy.yaml\n", stdout)
	ensure.Exactly("/test/is/ok", viper.Get("data-dir"))
	viper.Set("debug", false)
}

func Test_initConfig_AutodetectConfig(t *testing.T) {
	configFilePaths := []string{
		"/etc/budgy.yaml",
		"/etc/budgy/budgy.yaml",
		"$HOME/.budgy/budgy.yaml",
		"$HOME/budgy.yaml",
		"./budgy.yaml",
		".config/budgy.yaml",
	}
	for _, f := range configFilePaths {
		t.Run(f, func(t *testing.T) {
			ensure := assert.New(t)

			fs := afero.NewMemMapFs()
			viper.SetFs(fs)
			afero.WriteFile(fs, f, []byte(`data-dir: /test/is/ok`), 0644)

			initConfig()
			ensure.Exactly("/test/is/ok", viper.Get("data-dir"))
		})
	}
}

func Test_initConfig(t *testing.T) {
	tests := []struct {
		name           string
		filePath       string
		configContent  []byte
		validationFunc func(*assert.Assertions)
	}{
		{
			name:          "empty config file",
			filePath:      "/etc/budgy.yaml",
			configContent: []byte(""),
			validationFunc: func(ensure *assert.Assertions) {
				ensure.False(viper.GetBool("debug"))
				ensure.Exactly("", viper.Get("config-file"))
				ensure.Exactly("data/", viper.Get("data-dir"))
			},
		},
		{
			name:     "data-dir config option",
			filePath: "/etc/budgy/budgy.yaml",
			configContent: []byte(`
data-dir: /test/is/ok
`),
			validationFunc: func(ensure *assert.Assertions) {
				ensure.False(viper.GetBool("debug"))
				ensure.Exactly("", viper.Get("config-file"))
				ensure.Exactly("/test/is/ok", viper.Get("data-dir"))
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ensure := assert.New(t)

			fs := afero.NewMemMapFs()
			viper.SetFs(fs)
			afero.WriteFile(fs, tt.filePath, tt.configContent, 0644)

			cfgFile = tt.filePath
			initConfig()

			tt.validationFunc(ensure)
		})
	}
}
