// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"budgy/config"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
	"path/filepath"
	"strings"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   config.APPNAME,
	Short: "Manage your budget",
	Long:  `Web-based application to manage home budget and bank accounts`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func init() {
	// Define config file management
	cobra.OnInitialize(initConfig)

	// Configuration file to use
	viper.SetDefault("config-file", "")
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", fmt.Sprintf("config file, in yaml [%s_CONFIG]", config.EnvVarPrefix))
	if err := viper.BindPFlag("config-file", rootCmd.PersistentFlags().Lookup("config")); err != nil {
		panic(err)
	}

	// Debug mode
	viper.SetDefault("debug", false)
	rootCmd.PersistentFlags().BoolP("debug", "D", false, "Enable debug mode.")
	if err := viper.BindPFlag("debug", rootCmd.PersistentFlags().Lookup("debug")); err != nil {
		panic(err)
	}

	// Data folder
	viper.SetDefault("data-dir", "data/")
	rootCmd.PersistentFlags().String("data-dir", "data/", fmt.Sprintf("data directory [%s_DATADIR]", config.EnvVarPrefix))
	if err := viper.BindPFlag("data-dir", rootCmd.PersistentFlags().Lookup("data-dir")); err != nil {
		panic(err)
	}
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	// Configure config file auto detection
	viper.SetConfigType("yaml")
	viper.SetConfigName(config.APPNAME) // name of config file (without extension)
	viper.AddConfigPath("/etc")
	viper.AddConfigPath(filepath.Join("/etc", config.APPNAME))
	viper.AddConfigPath(filepath.Join("$HOME", config.APPNAME))
	viper.AddConfigPath("./config")
	viper.AddConfigPath(".")

	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	}

	// Setup environment variables handling
	viper.SetEnvPrefix(config.EnvVarPrefix)
	viper.AutomaticEnv()
	viper.AllowEmptyEnv(true)
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", ""))

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		if viper.GetBool("debug") {
			fmt.Println("Using config file:", viper.ConfigFileUsed())
		}
	}
}
