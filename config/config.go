// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// Package config contains the internal configuration of the program.
package config

// APPNAME contains the application name
var APPNAME = "budgy"

// VERSION contains the version of the program
var VERSION = "1.0.0+dev"

// REVISION contains the revision of the program
var REVISION = "HEAD"

// BUILDTIME contains the build date and time of the program
var BUILDTIME = ""

// RELEASE is true if the build binary is a release build (vs a debug/dev build if false
var RELEASE = true

// MainDBFileName is the name of the main database file of the application (only the name, not the path)
var MainDBFileName = APPNAME + ".db"

// EnvVarPrefix is a constant representing the PREFIX for all environment variables to configure the program
const EnvVarPrefix = "BGY"
