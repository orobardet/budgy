// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package i18n

import (
	"fmt"
	"github.com/GeertJohan/go.rice"
	"golang.org/x/text/language"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

var localesBox *rice.Box

func init() {
	InitI18N()
}

// InitI18N initialize localisation of the application and load localization files
func InitI18N() {
	localesBox = rice.MustFindBox("../locales")
	loadLocaleFiles()
}

func loadLocaleFiles() {
	localeFilesPerLanguage := scanLocaleFiles()

	for lang, files := range localeFilesPerLanguage {
		languageTag, err := language.Parse(lang)
		if err != nil {
			panic(err)
		}
		for _, file := range files {
			err := loadLocaleFile(languageTag, file)
			if err != nil {
				panic(err)
			}
		}

		// Declare the language localizer
		RegisterNewLocalizer(languageTag)
	}
}

func scanLocaleFiles() map[string][]string {
	localFilesPerLanguage := map[string][]string{}

	err := localesBox.Walk("", func(path string, info os.FileInfo, err error) error {
		if !info.Mode().IsRegular() {
			return nil
		}
		extension := strings.ToLower(filepath.Ext(path))
		if extension != ".yml" && extension != ".yaml" {
			return nil
		}
		pathSplit := strings.Split(path, string(os.PathSeparator))
		if len(pathSplit) <= 1 {
			return nil
		}

		lang := pathSplit[0]

		if _, exists := localFilesPerLanguage[lang]; !exists {
			localFilesPerLanguage[lang] = []string{}
		}
		localFilesPerLanguage[lang] = append(localFilesPerLanguage[lang], path)

		return nil
	})

	if err != nil {
		panic(err)
	}

	return localFilesPerLanguage
}

func loadLocaleFile(languageTag language.Tag, filePath string) error {
	f, err := localesBox.Open(filePath)
	if err != nil {
		return err
	}
	defer f.Close()

	bytes, _ := ioutil.ReadAll(f)

	localeDef := &TranslationsDefinition{}
	err = localeDef.LoadLocaleFile(bytes)
	if err != nil {
		return fmt.Errorf("Error while loading language filePath %s: %s", filePath, err)
	}

	err = localeDef.InjectStrings()
	if err != nil {
		return fmt.Errorf("Error while injecting language filePath %s: %s", filePath, err)
	}
	return nil
}
