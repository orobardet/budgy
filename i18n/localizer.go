// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package i18n

import (
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

// List of all the available localizer, which means all the language the application is localized to
var availableLocalizers = map[language.Tag]*message.Printer{}

// Localizer is the current selected localizer, which is the current language to localize the application
var Localizer *message.Printer

// RegisterNewLocalizer declare a new localizer for the given language.
// The declaration consists of creating (if it not already exists) a Printer for the language
// that will be available to localize the application for the given language.
func RegisterNewLocalizer(lang language.Tag) {
	if _, ok := availableLocalizers[lang]; !ok {
		availableLocalizers[lang] = message.NewPrinter(lang)
	}
}

// SetLocalizerLanguage set the current language to localize the application
func SetLocalizerLanguage(lang language.Tag) {

	if printer, ok := availableLocalizers[lang]; ok {
		Localizer = printer
	}
}

func init() {
	// TODO: set the current language programatically, defaulting to autodetect base on the environment locale
	SetLocalizerLanguage(language.French)
}
