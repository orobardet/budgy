// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package i18n

import (
	"golang.org/x/text/feature/plural"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"golang.org/x/text/message/catalog"
	"gopkg.in/yaml.v2"
	"strings"
)

// TranslationsDefinition represent a translation file (a local file) for a target language,
// containing localisation of messages
type TranslationsDefinition struct {
	// Target language string readed from local file
	Language string `yaml:"language"`
	// A list of message translation definition
	Messages []MessageDefinition `yaml:"messages,omitempty"`
	// Language tag, parsed from Language field
	LanguageTag language.Tag
}

// MessageDefinition represent the localisation of an application's message
type MessageDefinition struct {
	// The application message (the un-localized message, as used in the application)
	Message string `yaml:"message,omitempty"`
	// The translation of the message in the target language
	Translation string `yaml:"translation,omitempty"`
	// Optional list of variable definitions used in the translated message
	Vars []VarsDefinition `yaml:"vars,omitempty"`
}

// VarsDefinition represent a variable in a translated message
type VarsDefinition struct {
	// Name of the variable
	Name string `yaml:"name"`
	// Plural management definition associated to the variable
	Plural PluralDefinition
}

// PluralDefinition is a plural management definition for a translated message's variable
type PluralDefinition struct {
	// Index number (1 based) of the argument in the un-localized message, corresponding to the value that control the
	// plural of the variable
	ArgumentNumber int `yaml:"argumentNumber"`
	// Optional format of the argument value
	Format string `yaml:"format,omitempty"`
	// List of case rules to handle plural, depending of the value of the argument
	Cases []CaseDefinition `yaml:"cases"`
}

// CaseDefinition represent a rule for handling plural definition
type CaseDefinition struct {
	// Value of the argument the current rule matches
	Selector string `yaml:"selector"`
	// Value to use for the variable when the rule match
	Message string `yaml:"message"`
}

// LoadLocaleFile parse the content of an local file passed as argument, and load the data
// (metadata and all messages' translations) into the current TranslationsDefinition object
func (t *TranslationsDefinition) LoadLocaleFile(bytes []byte) error {
	err := yaml.UnmarshalStrict(bytes, t)
	if err != nil {
		return err
	}

	t.LanguageTag, err = language.Parse(t.Language)
	if err != nil {
		return err
	}

	return nil
}

// InjectStrings inject all messages translation of the current TranslationsDefinition object into the
// application's internal localized.
// Once injected, the application will be able to use the messages to localize the application.
func (t *TranslationsDefinition) InjectStrings() error {
	for _, msg := range t.Messages {
		catMsgs := []catalog.Message{}

		for _, varDef := range msg.Vars {
			if strings.TrimSpace(varDef.Name) == "" {
				continue
			}

			pluralCases := []interface{}{}
			for _, caseDef := range varDef.Plural.Cases {
				pluralCases = append(pluralCases, caseDef.Selector, caseDef.Message)
			}

			pluralVar := plural.Selectf(varDef.Plural.ArgumentNumber, varDef.Plural.Format, pluralCases...)
			msgVar := catalog.Var(varDef.Name, pluralVar)
			catMsgs = append(catMsgs, msgVar)
		}

		catMsgs = append(catMsgs, catalog.String(msg.Translation))
		err := message.Set(t.LanguageTag, msg.Message, catMsgs...)
		if err != nil {
			return err
		}
	}

	return nil
}
