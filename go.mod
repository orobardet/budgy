module budgy

//replace github.com/gin-gonic/gin => github.com/orobardet/gin v1.3.1-0.20190210105934-d42e4b379616

require (
	cloud.google.com/go v0.34.0 // indirect
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/GeertJohan/go.rice v0.0.0-20181229165343-eadc98236359
	github.com/daaku/go.zipexe v0.0.0-20150329023125-a5fe2436ffcb // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20190111225525-2fea367d496d // indirect
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect
	github.com/foolin/gin-template v0.0.0-20180515083052-089e62d1d723
	github.com/gin-contrib/sessions v0.0.0-20181124110724-7687b645ca6b
	github.com/gin-gonic/gin v1.3.1-0.20190303063943-0d50ce859745
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/google/go-cmp v0.2.0 // indirect
	github.com/gorilla/sessions v1.1.3 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jinzhu/gorm v1.9.2
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/jinzhu/now v0.0.0-20181116074157-8ec929ed50c3 // indirect
	github.com/kardianos/osext v0.0.0-20170510131534-ae77be60afb1 // indirect
	github.com/lib/pq v1.0.0 // indirect
	github.com/mattn/go-isatty v0.0.6 // indirect
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/quasoft/memstore v0.0.0-20180925164028-84a050167438 // indirect
	github.com/spf13/afero v1.1.2
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.1
	github.com/stretchr/testify v1.3.0
	golang.org/x/text v0.3.0
	google.golang.org/appengine v1.4.0 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
