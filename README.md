# Building

Requires [wellington](https://github.com/wellington/wellington/releases), a pure GO sass (fast!) compiler. 

```shell
# Compile sass to CSS
wt compile -b server/static/css --source-map server/static/sass/style.sass
```