// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package qif

import (
	"fmt"
	"time"
)

// Clearing value's enumeration
const (
	// No clearing
	ClearingNone = iota
	// The record is cleared
	ClearingCleared
	// The record is reconciled
	ClearingrReconciled
)

// BookRecord represent a book record
type BookRecord struct {
	ParseGIFLine
	Date         time.Time
	Amount       float64
	Payee        string
	PayeeAddress []string
	Category     string
	Note         string
	CheckNumber  string
	Clearing     int
	Split        []*RecordSplit
}

// NewBookRecord create and return a new empty BookRecord
func NewBookRecord() *BookRecord {
	return &BookRecord{}
}

// NewBookRecordParsed create a new BookRecord populated with information parsed in QIF record given in
// argument as an array of strings.
// The BookRecord is then returned
// Uses ParseQIFRecord.
func NewBookRecordParsed(qifRecord []string) *BookRecord {
	record := NewBookRecord()
	record.ParseQIFRecord(qifRecord)
	return record
}

// ParseQIFRecord analyse a QIF record given in argument as an array of strings, and populate the
// current BookRecord with the extracted information
func (r *BookRecord) ParseQIFRecord(qifRecord []string) {
	var currentSplit *RecordSplit
	for _, line := range qifRecord {
		switch line[0] {
		// Date
		case 'D':
			r.ParseQIFDate(line[1:], &r.Date)
		// Amount
		case 'T', 'U':
			r.ParseQIFAmount(line[1:], &r.Amount)
		// Note, comment
		case 'M':
			r.Note = line[1:]
		// Clearing status
		case 'C':
			r.ParseQIFClearing(line[1:], &r.Clearing)
		// Check number
		case 'N':
			r.CheckNumber = line[1:]
		// Payee
		case 'P':
			r.Payee = line[1:]
		// Address of the payee
		case 'A':
			r.PayeeAddress = append(r.PayeeAddress, line[1:])
		// Category
		case 'L':
			r.Category = line[1:]
		// Split category
		case 'S':
			if currentSplit != nil {
				r.AddSplit(currentSplit)
				currentSplit = nil
			}
			currentSplit = NewRecordSplit()
			currentSplit.Category = line[1:]
		// Split note, comment
		case 'E':
			if currentSplit != nil {
				currentSplit.Note = line[1:]
			}
		// Split amount
		case '$':
			if currentSplit != nil {
				r.ParseQIFAmount(line[1:], &currentSplit.Amount)
			}
		default:
			fmt.Printf("Unknown record: %s\n", line)
		}
	}
	if currentSplit != nil {
		r.AddSplit(currentSplit)
		currentSplit = nil
	}
}

// AddSplit add a given split to the record split list
func (r *BookRecord) AddSplit(s *RecordSplit) {
	r.Split = append(r.Split, s)
}
