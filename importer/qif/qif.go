// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package qif

import (
	"bufio"
	"fmt"
	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

// ParseGIFLine is a common struct for parsing GIF record line
type ParseGIFLine struct{}

// ParseQIFDate parse a QIF date is s, and if successful set the output
func (p ParseGIFLine) ParseQIFDate(s string, output *time.Time) {
	t, err := time.Parse("02/01'2006", s)
	if err == nil {
		*output = t
	}
}

// ParseQIFAmount parse a QIF amount is s, and if successful set the output
func (p ParseGIFLine) ParseQIFAmount(s string, output *float64) {
	amount, err := strconv.ParseFloat(strings.Replace(s, ",", "", -1), 32)
	if err == nil {
		*output = amount
	}
}

// ParseQIFClearing parse a QIF amount is s, and if successful set the output
func (p ParseGIFLine) ParseQIFClearing(s string, output *int) {
	switch s[0] {
	case '*':
		*output = ClearingCleared
	case 'X':
		*output = ClearingrReconciled
	case 'R':
		*output = ClearingrReconciled
	default:
		*output = ClearingNone
	}
}

// Importer is a QIF importer object
type Importer struct {
	ParseGIFLine
	linesCount   uint64
	recordsCount uint64
	scanner      *bufio.Scanner
}

// NewImporter create and return a new QIF Importer object
func NewImporter() *Importer {
	return &Importer{}
}

// ImportFile run the import of a QIF file whose path is given as argument.
// Uses ImportReader
func (i *Importer) ImportFile(filePath string) error {
	reader, err := os.Open(filePath) // #nosec G304
	if err != nil {
		return fmt.Errorf("error while opening QIF file %s: %s", filePath, err)
	}
	defer reader.Close()

	// TODO: use detection or a parameter of the import to specify the input file encoding
	convertedReader := transform.NewReader(reader, charmap.Windows1252.NewDecoder())
	return i.ImportReader(convertedReader)
}

// ImportReader import QIF data represented by an io.Reader given as argument.
func (i *Importer) ImportReader(reader io.Reader) error {
	i.scanner = bufio.NewScanner(reader)

	log.SetOutput(ioutil.Discard)
	log.SetFlags(0)

	i.linesCount = 0
	i.recordsCount = 0
	for line, ok := i.readNextLine(); ok; line, ok = i.readNextLine() {
		switch line {
		case "!Type:Bank":
			bankBook := i.readBankType()
			// TODO: don't just display the book, use it
			fmt.Printf("%v\n", bankBook)
		default:
			return fmt.Errorf("unable to find a known QIF format for \"%s\"", line)
		}
	}

	fmt.Printf("%d lines readed\n", i.linesCount)
	fmt.Printf("%d records readed\n", i.recordsCount)

	return nil
}

func (i *Importer) readNextLine() (string, bool) {
	if i.scanner.Scan() {
		i.linesCount++
		line := strings.TrimSpace(i.scanner.Text())
		log.Printf("%5d: %s\n", i.linesCount, line)
		return line, true
	}

	return "", false
}

func (i *Importer) readBankType() *BankBook {

	bankBook := NewBankBook()

	bankAccountOptions, ok := i.readNextRecord()
	if ok {
		bankBook.ParseQIFRecord(bankAccountOptions)
	}

	for record, ok := i.readNextRecord(); ok; record, ok = i.readNextRecord() {
		bankBook.ParseAndAddRecord(record)
	}

	return bankBook
}

func (i *Importer) readNextRecord() ([]string, bool) {
	var record []string
	for line, ok := i.readNextLine(); ok; line, ok = i.readNextLine() {
		if line == "^" {
			i.recordsCount++
			return record, true
		}

		record = append(record, line)
	}

	return record, false
}
