// Copyright © 2018-2019 Olivier Robardet
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package qif

import (
	"fmt"
	"strings"
	"time"
)

// BankBook describe a bank's book
type BankBook struct {
	ParseGIFLine
	Name         string
	CreationDate time.Time
	Records      []*BookRecord
}

// NewBankBook create and return a new empty bank's book
func NewBankBook() *BankBook {
	return &BankBook{}
}

// ParseQIFRecord analyse a QIF record, represented by a list of string, and
// extract information to populate the book's details
func (b *BankBook) ParseQIFRecord(qifRecord []string) {
	for _, line := range qifRecord {
		switch line[0] {
		case 'L':
			b.Name = strings.Trim(line[1:], "[]")
		case 'D':
			b.ParseQIFDate(line[1:], &b.CreationDate)
		}
	}
}

// AddRecord append a given record in the bank's book records list
func (b *BankBook) AddRecord(record *BookRecord) {
	b.Records = append(b.Records, record)
}

// ParseAndAddRecord analyse a QIF record, represented by a list of string, create a
// book record from it, and add this record in the bank's book records list
func (b *BankBook) ParseAndAddRecord(qifRecord []string) {
	b.Records = append(b.Records, NewBookRecordParsed(qifRecord))
}

// ComputeCurrentBalance compute and return the current book balance
func (b *BankBook) ComputeCurrentBalance() float64 {
	var balance float64
	for _, r := range b.Records {
		balance += r.Amount
	}

	return balance
}

// String return the string representation of a BankBook object
func (b *BankBook) String() string {
	return fmt.Sprintf("Bank book \"%s\", containing %d records, since %s ; balance: %.2f", b.Name, len(b.Records), b.CreationDate, b.ComputeCurrentBalance())
}
